use std::collections::HashSet;
use std::fmt::Write;
use std::time::Duration;
use time::OffsetDateTime;

fn secs(secs: u64) -> Duration {
    Duration::from_secs(secs)
}

#[derive(Debug, Clone, Eq)]
pub struct Timer {
    pub dur: Duration,
    pub msg: Option<String>,
}

impl Timer {
    pub fn as_secs(&self) -> u64 {
        self.dur.as_secs()
    }
}

impl PartialEq for Timer {
    fn eq(&self, other: &Self) -> bool {
        self.dur == other.dur
    }
}

fn has_duplicates(timers: &[Timer]) -> bool {
    let mut uniq = HashSet::new();
    timers.iter().any(|ti| !uniq.insert(ti.dur))
}

peg::parser!(pub grammar parse() for str {
    rule natural() -> u64 = n:$(['1'..='9']['0'..='9']*) { n.parse().unwrap() }
    rule float() -> f32
        = f:$(['0'..='9']"."['1'..='9']) { f.parse().unwrap() }
        / f:$(natural()) { f.parse().unwrap() }
    rule hours() -> u64 = n:float()"h" { (n * 3600.0) as u64 }
    rule minutes() -> u64
        = m:float()"m" { (m * 60.0) as u64 }
        / m:float()&"{" { (m * 60.0) as u64 }
        / m:float()![_] { (m * 60.0) as u64 }
    rule seconds() -> u64 = s:natural()"s" { s }
    pub rule hms2dur() -> Duration
        = h:hours() m:minutes() s:seconds() { secs(h + m + s) }
        / h:hours() m:minutes() { secs(h + m) }
        / h:hours() s:seconds() { secs(h + s) }
        / h:hours() { secs(h) }
        / m:minutes() s:seconds() { secs(m + s) }
        / m:minutes() { secs(m) }
        / s:seconds() { secs(s) }
    pub rule msg() -> String = "{" s:$([^'}']+) "}" { s.into() }
    pub rule timer_hms() -> Timer = dur:hms2dur() msg:msg()? { Timer { dur, msg } }
    rule checkp_from_start() -> Timer = "-" ti:timer_hms() { ti }
    rule checkp_exact() -> Timer = "=" ti:timer_hms() { ti }
    pub rule hms_with_checkp() -> (Timer, Vec<Timer>)
        = d:timer_hms() cs:checkp_from_start()* ce:checkp_exact()* {?
            let mut cs = cs;
            let mut ce = ce;
            ce.iter_mut().for_each(|ti| ti.dur = d.dur - ti.dur);
            cs.append(&mut ce);
            if has_duplicates(&cs) {
                return Err("duplicate checkpoints");
            }
            if cs.iter().any(|ti| ti.dur >= d.dur || ti.dur == secs(0)) {
                return Err("invalid timer");
            }
            Ok((d, cs))
        }

    pub rule hour() -> u8
        = h:$("2"['0'..='3']) { h.parse().unwrap() }
        / h:$("1"['0'..='9']) { h.parse().unwrap() }
        / h:$("0"['0'..='9']) { h.parse().unwrap() }
        / h:$(['0'..='9']) { h.parse().unwrap() }
    pub rule minute() -> u8 = m:$(['0'..='5']['0'..='9']) { m.parse().unwrap() }
    pub rule hm()  -> (u8, u8) = h:hour() ":" m:minute() { (h, m) }
    pub rule dur_till() -> Duration = hm:hm() {
        let t1 = if cfg!(test) {
            OffsetDateTime::UNIX_EPOCH
                .saturating_add(time::Duration::DAY * 364 + time::Duration::HOUR * 23)
        } else {
            OffsetDateTime::now_local().unwrap()
        };
        let (h, m) = hm;
        let mut t2 = t1.replace_hour(h).unwrap().replace_minute(m).unwrap().replace_second(0).unwrap();
        let mut dur = t2 - t1;
        if dur.is_negative() {
            t2 = t2.saturating_add(time::Duration::DAY);
            dur = t2 - t1;
        };
        secs(dur.whole_seconds() as u64)
    }
    pub rule timer_till() -> Timer = dur:dur_till() msg:msg()? { Timer { dur, msg } }

    pub rule timespec() -> (Timer, Vec<Timer>)
        = tt:timer_till() { (tt, vec![]) }
        / hms_with_checkp()

        #[no_eof]
        pub rule raw_timepoint() -> String = raw:$(['0'..='9'|':'|'.'|'h'|'m'|'s']+) { raw.into() }
});

pub fn secs2hms(secs: u64) -> String {
    let (hours, rem) = (secs / 3600, secs % 3600);
    let (mins, secs) = (rem / 60, rem % 60);
    let mut result = String::with_capacity(8);
    if hours > 0 {
        write!(result, "{hours}h").unwrap();
    }
    if mins > 0 {
        write!(result, "{mins}m").unwrap();
    }
    if secs > 0 {
        write!(result, "{secs}s").unwrap();
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! timer {
        ($sec:expr,$msg:expr) => {
            Timer {
                dur: secs($sec),
                msg: Some($msg.to_string()),
            }
        };
        ($sec:expr) => {
            Timer {
                dur: secs($sec),
                msg: None,
            }
        };
    }

    #[test]
    fn test_raw_timepoint() {
        assert_eq!(parse::raw_timepoint("1h1.5m"), Ok("1h1.5m".to_string()));
        assert_eq!(parse::raw_timepoint("1h1m1s"), Ok("1h1m1s".to_string()));
        assert_eq!(parse::raw_timepoint("1h1m1s-5s"), Ok("1h1m1s".to_string()));
        assert_eq!(parse::raw_timepoint("1h1m1s=5s"), Ok("1h1m1s".to_string()));
        assert_eq!(
            parse::raw_timepoint("14:00{alarm!}"),
            Ok("14:00".to_string())
        );
    }
    #[test]
    fn test_timespec() {
        assert_eq!(
            parse::timespec("10m-4m=1m"),
            Ok((timer!(600), vec![timer!(240), timer!(540)]))
        );
        assert_eq!(
            parse::timespec("10m=4m"),
            Ok((timer!(600), vec![timer!(360)]))
        );
        assert_eq!(
            parse::timespec("1h10m10s"),
            Ok((timer!(3600 + 600 + 10), vec![]))
        );
        assert_eq!(
            parse::timespec("1h10m10s-0.5h0.5m5s"),
            Ok((timer!(3600 + 600 + 10), vec![timer!(1835)],))
        );
        assert_eq!(
            parse::timespec("1:00{alarm!}"),
            Ok((timer!(3600 * 2, "alarm!"), vec![]))
        );
        assert!(parse::timespec("4:00-10s").is_err());
        assert!(parse::timespec("10s-").is_err());
        assert!(parse::timespec("10m=").is_err());
        assert!(parse::timespec("10m-=10s").is_err());
        assert!(parse::timespec("1.5s").is_err());
    }
    #[test]
    fn test_timer_till() {
        assert_eq!(
            parse::timer_till("1:00{alarm}"),
            Ok(timer!(2 * 3600, "alarm"))
        );
    }
    #[test]
    fn test_duplicate_checkps() {
        assert!(parse::hms_with_checkp("1h-1m{foo}-60s{bar}").is_err());
        assert!(parse::timespec("10m-3m=7m").is_err());
    }
    #[test]
    fn test_hms_with_checkp() {
        assert_eq!(
            parse::hms_with_checkp("1{foo}"),
            Ok((timer!(60, "foo"), vec![]))
        );
        assert_eq!(
            parse::hms_with_checkp("1m{foo}-5s{bar}=20s{baz}"),
            Ok((timer!(60, "foo"), vec![timer!(5, "bar"), timer!(40, "baz")]))
        );
        assert!(parse::hms_with_checkp("1-5s").is_err());
        assert!(parse::hms_with_checkp("9s-10s").is_err());
        assert!(parse::hms_with_checkp("1m-0s").is_err());
    }
    #[test]
    fn test_dur_till() {
        assert_eq!(parse::hour("9"), Ok(9));
        assert_eq!(parse::hour("09"), Ok(9));
        assert_eq!(parse::hour("23"), Ok(23));
        assert!(parse::hour("24").is_err());

        assert_eq!(parse::minute("00"), Ok(0));
        assert_eq!(parse::minute("59"), Ok(59));
        assert!(parse::minute("0").is_err());
        assert!(parse::minute("60").is_err());

        assert_eq!(parse::hm("0:00"), Ok((0, 0)));
        assert_eq!(parse::hm("4:00"), Ok((4, 0)));
        assert_eq!(parse::hm("14:03"), Ok((14, 3)));
        assert_eq!(parse::hm("23:59"), Ok((23, 59)));
        assert!(parse::hm("14:3").is_err());
        assert!(parse::hm("143").is_err());
        assert!(parse::hm("24:00").is_err());

        assert_eq!(parse::dur_till("4:00"), Ok(secs(3600 * 5)));
        assert_eq!(parse::dur_till("0:30"), Ok(secs(3600 + 60 * 30)));
        assert_eq!(parse::dur_till("4:44"), Ok(secs(3600 * 5 + 44 * 60)));
    }
    #[test]
    fn test_timer_hms() {
        assert_eq!(parse::timer_hms("1h"), Ok(timer!(3600)));
        assert_eq!(
            parse::timer_hms("1m{a,b.c!d-e=}"),
            Ok(timer!(60, "a,b.c!d-e="))
        );
    }
    #[test]
    fn test_msg() {
        assert_eq!(parse::msg("{hello}"), Ok("hello".to_string()));
    }
    #[test]
    fn test_hms2dur() {
        assert!(parse::hms2dur("0").is_err());
        assert!(parse::hms2dur("0m").is_err());
        assert_eq!(parse::hms2dur("1"), Ok(secs(60)));
        assert_eq!(parse::hms2dur("1h"), Ok(secs(3600)));
        assert_eq!(parse::hms2dur("1m"), Ok(secs(60)));
        assert_eq!(parse::hms2dur("1m10s"), Ok(secs(70)));
        assert_eq!(parse::hms2dur("1.5h"), Ok(secs(5400)));
    }
    #[test]
    fn test_dur2hms() {
        assert_eq!(secs2hms(3600 + 10 * 60 + 10), "1h10m10s");
        assert_eq!(secs2hms(10 * 60 + 10), "10m10s");
        assert_eq!(secs2hms(10), "10s");
        assert_eq!(secs2hms(3600), "1h");
        assert_eq!(secs2hms(60), "1m");
        assert_eq!(secs2hms(0), "");
    }
}
