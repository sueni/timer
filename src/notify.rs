use std::borrow::Cow;
use std::io::{self, Write};
use std::process::{Command, Stdio};

const START_WAV: &[u8] = include_bytes!("start.wav");
const END_WAV: &[u8] = include_bytes!("end.wav");
const NOTIFIER: &str = "dunstify";

fn aplay(adata: &[u8]) -> io::Result<()> {
    Command::new("aplay")
        .arg("-q")
        .stdin(Stdio::piped())
        .spawn()?
        .stdin
        .map(|mut s| s.write_all(adata));
    Ok(())
}

pub fn start(hms: &str, msg: Option<&str>) {
    let mut notify = Command::new(NOTIFIER);
    notify.arg(hms);
    if let Some(msg) = msg {
        notify.arg(msg);
    }
    _ = notify.spawn();
    _ = aplay(START_WAV);
}

pub fn checkpoint(hms: &str, msg: Option<&str>, raw: &str) {
    let mut notify = Command::new(NOTIFIER);
    notify.args([
        "-t",
        "0",
        "-h",
        "string:fgcolor:#cfb2f3",
        "-h",
        "string:bgcolor:#5f00d7",
        hms,
    ]);
    let body = match msg {
        Some(msg) => Cow::from(format!("<i>{msg}</i>\n{raw}")),
        None => Cow::from(raw),
    };
    _ = notify.arg(&*body).spawn();
}

pub fn finish(raw: &str, msg: Option<&str>) {
    let mut notify = Command::new(NOTIFIER);
    notify.args([
        "-t",
        "0",
        "-h",
        "string:fgcolor:#ffff00",
        "-h",
        "string:bgcolor:#f90038",
        raw,
    ]);
    if let Some(msg) = msg {
        notify.arg(msg);
    }
    _ = notify.spawn();
    _ = aplay(END_WAV);
}

pub fn green(title: &str, body: &str) {
    _ = Command::new(NOTIFIER)
        .args([
            "-h",
            "string:fgcolor:#184729",
            "-h",
            "string:bgcolor:#54ff94",
            title,
            body,
        ])
        .spawn();
}

pub fn yellow(title: &str, body: &str) {
    _ = Command::new(NOTIFIER)
        .args([
            "-h",
            "string:fgcolor:#473d00",
            "-h",
            "string:bgcolor:#ffd900",
            title,
            body,
        ])
        .spawn();
}

pub fn red(title: &str, body: &str) {
    _ = Command::new(NOTIFIER)
        .args([
            "-h",
            "string:fgcolor:#ffff00",
            "-h",
            "string:bgcolor:#f90038",
            title,
            body,
        ])
        .spawn();
}
