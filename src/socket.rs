use std::fs;
use std::io::{self, Write};
use std::os::unix::fs::FileTypeExt;
use std::os::unix::net::UnixStream;
use std::path::{Path, PathBuf};

pub fn dir() -> io::Result<PathBuf> {
    let dir = PathBuf::from("/tmp/timer-socks");
    if !dir.is_dir() {
        std::fs::create_dir(&dir)?;
    }
    Ok(dir)
}

pub fn new() -> io::Result<PathBuf> {
    Ok(dir()?.join(std::process::id().to_string()))
}

pub fn files() -> io::Result<impl Iterator<Item = PathBuf>> {
    Ok(fs::read_dir(dir()?)?
        .into_iter()
        .flatten()
        .filter(|f| f.metadata().unwrap().file_type().is_socket())
        .map(|f| f.path()))
}

pub fn recent() -> Option<PathBuf> {
    if let Ok(files) = files() {
        return files.max_by_key(|f| f.metadata().unwrap().modified().unwrap());
    }
    None
}

pub fn write(sock: &Path, msg: &[u8]) -> io::Result<()> {
    let mut s = UnixStream::connect(sock)?;
    s.write_all(msg)?;
    Ok(())
}
