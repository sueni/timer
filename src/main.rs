use crate::time::Timer;
use fork::{daemon, Fork};
use std::io::{self, BufRead, BufReader};
use std::os::unix::net::UnixListener;
use std::path::PathBuf;
use std::thread;
use std::time::Instant;

mod notify;
mod socket;
mod time;

const HELP: &str = "Usage: timer <command(s)>
Commands:
    list
    undo
    #h#m#s[{MSG}][-#h#m#s[{MSG}]][=#h#m#s[{MSG}]] <exec-at-end>
    H:M[{MSG}] <exec-at-end>";

fn run_new_timer(
    start: Instant,
    raw_timepoint: String,
    timer: Timer,
    sock: PathBuf,
    checkps: Vec<Timer>,
    postcmd: Option<String>,
) -> io::Result<()> {
    if let Ok(Fork::Child) = daemon(false, true) {
        let listener = UnixListener::bind(&sock)?;
        let sock_ = sock.clone();
        let main_dur = timer.dur;
        let main_msg = &timer.msg;
        let main_secs = timer.as_secs();
        let raw_timepoint_ = raw_timepoint.clone();

        notify::start(&time::secs2hms(timer.as_secs()), main_msg.as_deref());
        thread::spawn(move || {
            for stream in listener.incoming().flatten() {
                let stream = BufReader::new(stream);
                for line in stream.lines().flatten() {
                    match line.as_ref() {
                        "terminate" => {
                            std::fs::remove_file(sock_).unwrap();
                            std::process::exit(0);
                        }
                        "status" => {
                            let timeout_left = (main_dur - start.elapsed()).as_secs();
                            match main_secs.checked_div(timeout_left) {
                                Some(10..) => {
                                    notify::red(&time::secs2hms(timeout_left), &raw_timepoint_)
                                }
                                Some(3..=9) => {
                                    notify::yellow(&time::secs2hms(timeout_left), &raw_timepoint_)
                                }
                                Some(_) => {
                                    notify::green(&time::secs2hms(timeout_left), &raw_timepoint_)
                                }
                                None => (),
                            }
                        }
                        _ => (),
                    }
                }
            }
        });

        thread::scope(|s| {
            for ti in &checkps {
                s.spawn(|| {
                    thread::sleep(ti.dur - start.elapsed());
                    notify::checkpoint(
                        &time::secs2hms(ti.as_secs()),
                        ti.msg.as_deref(),
                        main_msg.as_ref().unwrap_or(&raw_timepoint),
                    );
                });
            }
        });
        thread::sleep(main_dur - start.elapsed());

        if let Some(postcmd) = postcmd {
            _ = std::process::Command::new("sh")
                .arg("-c")
                .arg(postcmd)
                .spawn();
        }
        notify::finish(&raw_timepoint, main_msg.as_deref());
        std::fs::remove_file(sock)?;
    }
    Ok(())
}

fn list_timers() -> io::Result<()> {
    for sock in socket::files()? {
        socket::write(&sock, b"status")?
    }
    Ok(())
}

fn undo_last_timer() -> io::Result<()> {
    if let Some(sock) = socket::recent() {
        socket::write(&sock, b"terminate")?
    }
    Ok(())
}

macro_rules! abort {
    ($e:expr) => {{
        eprintln!("{}", $e);
        std::process::exit(1)
    }};
    () => {
        std::process::exit(1)
    };
}

fn main() -> io::Result<()> {
    let mut args = std::env::args().skip(1);
    match args.next().as_deref() {
        Some("list") => list_timers()?,
        Some("undo") => undo_last_timer()?,
        Some(arg) => match time::parse::timespec(arg) {
            Ok((timeout, checkps)) => {
                let raw_timepoint = time::parse::raw_timepoint(arg).unwrap();
                let postcmd = args.next();
                run_new_timer(
                    Instant::now(),
                    raw_timepoint,
                    timeout,
                    socket::new()?,
                    checkps,
                    postcmd,
                )?;
            }
            Err(_) => abort!(),
        },
        None => abort!(HELP),
    };
    Ok(())
}
